﻿namespace LogReader
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.cb_InputCalls = new System.Windows.Forms.CheckBox();
            this.cb_OutputCity = new System.Windows.Forms.CheckBox();
            this.cb_SpecialService = new System.Windows.Forms.CheckBox();
            this.cb_OutputIntercity = new System.Windows.Forms.CheckBox();
            this.cb_OutputInternational = new System.Windows.Forms.CheckBox();
            this.l_StringsParsed = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.l_AllStrings = new System.Windows.Forms.Label();
            this.cb_Debug = new System.Windows.Forms.CheckBox();
            this.b_Start = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(12, 245);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(163, 58);
            this.button1.TabIndex = 0;
            this.button1.Text = "Обзор...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(8, 212);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Выберите файлы";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(282, 320);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(209, 23);
            this.progressBar1.Step = 1;
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 5;
            this.progressBar1.Visible = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 320);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(251, 23);
            this.label2.TabIndex = 6;
            this.label2.UseMnemonic = false;
            this.label2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(69, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(381, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Обработка cdr-файлов от станции M-200 (СОРМ)";
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(325, 245);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(166, 58);
            this.button2.TabIndex = 14;
            this.button2.Text = "Выгрузить в Excel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "|*.csv;*.*";
            this.saveFileDialog1.RestoreDirectory = true;
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // cb_InputCalls
            // 
            this.cb_InputCalls.AutoSize = true;
            this.cb_InputCalls.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cb_InputCalls.Location = new System.Drawing.Point(35, 69);
            this.cb_InputCalls.Name = "cb_InputCalls";
            this.cb_InputCalls.Size = new System.Drawing.Size(163, 22);
            this.cb_InputCalls.TabIndex = 15;
            this.cb_InputCalls.Text = "Входящие от МГТС";
            this.cb_InputCalls.UseVisualStyleBackColor = true;
            this.cb_InputCalls.CheckedChanged += new System.EventHandler(this.cb_InputCalls_CheckedChanged);
            // 
            // cb_OutputCity
            // 
            this.cb_OutputCity.AutoSize = true;
            this.cb_OutputCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cb_OutputCity.Location = new System.Drawing.Point(35, 98);
            this.cb_OutputCity.Name = "cb_OutputCity";
            this.cb_OutputCity.Size = new System.Drawing.Size(378, 22);
            this.cb_OutputCity.TabIndex = 16;
            this.cb_OutputCity.Text = "Исходящие на МГТС (100, \"495\", \"498\", \"499\", \"800\")";
            this.cb_OutputCity.UseVisualStyleBackColor = true;
            this.cb_OutputCity.CheckedChanged += new System.EventHandler(this.cb_OutputCity_CheckedChanged);
            // 
            // cb_SpecialService
            // 
            this.cb_SpecialService.AutoSize = true;
            this.cb_SpecialService.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cb_SpecialService.Location = new System.Drawing.Point(35, 126);
            this.cb_SpecialService.Name = "cb_SpecialService";
            this.cb_SpecialService.Size = new System.Drawing.Size(269, 22);
            this.cb_SpecialService.TabIndex = 17;
            this.cb_SpecialService.Text = "Спецслужбы (\"01\"-\"04\", \"101\"-\"104\")";
            this.cb_SpecialService.UseVisualStyleBackColor = true;
            this.cb_SpecialService.CheckedChanged += new System.EventHandler(this.cb_SpecialService_CheckedChanged);
            // 
            // cb_OutputIntercity
            // 
            this.cb_OutputIntercity.AutoSize = true;
            this.cb_OutputIntercity.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cb_OutputIntercity.Location = new System.Drawing.Point(35, 154);
            this.cb_OutputIntercity.Name = "cb_OutputIntercity";
            this.cb_OutputIntercity.Size = new System.Drawing.Size(492, 22);
            this.cb_OutputIntercity.TabIndex = 18;
            this.cb_OutputIntercity.Text = "Исходящие междугородние (звонки по России и мобильные сети)";
            this.cb_OutputIntercity.UseVisualStyleBackColor = true;
            this.cb_OutputIntercity.CheckedChanged += new System.EventHandler(this.cb_OutputIntercity_CheckedChanged);
            // 
            // cb_OutputInternational
            // 
            this.cb_OutputInternational.AutoSize = true;
            this.cb_OutputInternational.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cb_OutputInternational.Location = new System.Drawing.Point(35, 182);
            this.cb_OutputInternational.Name = "cb_OutputInternational";
            this.cb_OutputInternational.Size = new System.Drawing.Size(273, 22);
            this.cb_OutputInternational.TabIndex = 19;
            this.cb_OutputInternational.Text = "Исходящие международные (\"810\")";
            this.cb_OutputInternational.UseVisualStyleBackColor = true;
            this.cb_OutputInternational.CheckedChanged += new System.EventHandler(this.cb_OutputInternational_CheckedChanged);
            // 
            // l_StringsParsed
            // 
            this.l_StringsParsed.AutoSize = true;
            this.l_StringsParsed.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.l_StringsParsed.Location = new System.Drawing.Point(12, 359);
            this.l_StringsParsed.Name = "l_StringsParsed";
            this.l_StringsParsed.Size = new System.Drawing.Size(167, 18);
            this.l_StringsParsed.TabIndex = 20;
            this.l_StringsParsed.Text = "Не обработано строк: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 18);
            this.label4.TabIndex = 21;
            this.label4.Text = "1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(12, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 18);
            this.label5.TabIndex = 22;
            this.label5.Text = "2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(13, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 18);
            this.label6.TabIndex = 23;
            this.label6.Text = "3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(13, 154);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 18);
            this.label7.TabIndex = 24;
            this.label7.Text = "4";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(13, 183);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 18);
            this.label8.TabIndex = 25;
            this.label8.Text = "5";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button3.Location = new System.Drawing.Point(32, 34);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(119, 31);
            this.button3.TabIndex = 26;
            this.button3.Text = "Выбрать всё";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // l_AllStrings
            // 
            this.l_AllStrings.AutoSize = true;
            this.l_AllStrings.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.l_AllStrings.Location = new System.Drawing.Point(297, 359);
            this.l_AllStrings.Name = "l_AllStrings";
            this.l_AllStrings.Size = new System.Drawing.Size(101, 18);
            this.l_AllStrings.TabIndex = 27;
            this.l_AllStrings.Text = "Всего строк: ";
            // 
            // cb_Debug
            // 
            this.cb_Debug.AutoSize = true;
            this.cb_Debug.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cb_Debug.Location = new System.Drawing.Point(379, 39);
            this.cb_Debug.Name = "cb_Debug";
            this.cb_Debug.Size = new System.Drawing.Size(137, 22);
            this.cb_Debug.TabIndex = 28;
            this.cb_Debug.Text = "Режим отладки";
            this.cb_Debug.UseVisualStyleBackColor = true;
            // 
            // b_Start
            // 
            this.b_Start.Enabled = false;
            this.b_Start.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.b_Start.Location = new System.Drawing.Point(193, 245);
            this.b_Start.Name = "b_Start";
            this.b_Start.Size = new System.Drawing.Size(115, 58);
            this.b_Start.TabIndex = 29;
            this.b_Start.Text = "Старт";
            this.b_Start.UseVisualStyleBackColor = true;
            this.b_Start.Click += new System.EventHandler(this.b_Start_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 385);
            this.Controls.Add(this.b_Start);
            this.Controls.Add(this.cb_Debug);
            this.Controls.Add(this.l_AllStrings);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.l_StringsParsed);
            this.Controls.Add(this.cb_OutputInternational);
            this.Controls.Add(this.cb_OutputIntercity);
            this.Controls.Add(this.cb_SpecialService);
            this.Controls.Add(this.cb_OutputCity);
            this.Controls.Add(this.cb_InputCalls);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "LogReader";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.CheckBox cb_InputCalls;
        private System.Windows.Forms.CheckBox cb_OutputCity;
        private System.Windows.Forms.CheckBox cb_SpecialService;
        private System.Windows.Forms.CheckBox cb_OutputIntercity;
        private System.Windows.Forms.CheckBox cb_OutputInternational;
        private System.Windows.Forms.Label l_StringsParsed;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label l_AllStrings;
        private System.Windows.Forms.CheckBox cb_Debug;
        private System.Windows.Forms.Button b_Start;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}


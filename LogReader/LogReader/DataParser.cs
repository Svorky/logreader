﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Linq;
using Extensions;
using System.Diagnostics;

namespace LogReader
{
    class DataParser
    {
        public enum Filter
        {
            NotFiltered = 0,
            InputCalls = 1,
            OutputCityCalls = 2,
            SpecialServiceCalls = 3,
            OutputCrossCityCalls = 4,
            OutputCrossCountryCalls = 5
        }
        public static List<Filter> fl = new List<Filter>();

        public static void Parser()
        {
            fl.Sort();
            Regex abonentFilter = null;
            Regex numberFilter = null;
            int column = -1;
            DataBase.AddNewTable("Result");
            var query = DataBase.dataset.Tables["Data"].AsEnumerable().GroupBy(row => row[6]);
            foreach (var q in query)
            {
                DataRow resRow = DataBase.dataset.Tables["Result"].NewRow();
                resRow.SetFieldWithColumn(0, q.Key);
                int index = 0;
                foreach (var check in fl)
                {
                    index++;
                    switch ((int) check)
                    {
                        case 1: //Входящие
                            {
                                abonentFilter = new Regex(@"^C12701[0-9]\d+"); //Фильтр для входящих абонентов
                                column = 1;
                                var w = from row in q
                                        where abonentFilter.IsMatch(row[0].ToString())
                                        select row;
                                var sum = from e in w.AsEnumerable()
                                          select e[9].ToDecimal();
                                foreach(DataRow row in w)
                                {
                                    row.SetFieldWithColumn(11, "Filtered");
                                }
                                //Debug.WriteLine("Vhod: " + sum.Sum());
                                resRow.SetFieldWithColumn(index, Math.Round(sum.Sum() / 60, 2));
                                break;
                            }
                        case 2: //Исходящие на МГТС
                            {
                                abonentFilter = new Regex(@"C12700[0-9]*"); //Фильтр для исходящих абонентов
                                numberFilter = new Regex(@"^(100|8495|8498|8499|8800)");//Городские
                                column = 2;
                                var w = from row in q
                                        where abonentFilter.IsMatch(row[0].ToString())
                                        where row[column].ToString().Length >= 11 || row[column].ToString().Length == 3
                                        where numberFilter.IsMatch(row[column].ToString())
                                        select row;
                                var sum = from e in w.AsEnumerable()
                                          select e[8].ToDecimal();
                                foreach (DataRow row in w)
                                {
                                    row.SetFieldWithColumn(11, "Filtered");
                                }
                                //Debug.WriteLine("MGTS: " + sum.Sum());
                                resRow.SetFieldWithColumn(index, Math.Round(sum.Sum() / 60, 2));
                                break;
                            }
                        case 3:
                            {
                                abonentFilter = new Regex(@"C12700[0-9]*"); //Фильтр для исходящих абонентов
                                numberFilter = new Regex(@"^(01|02|03|04|101|102|103|104)");//Спецслужбы
                                column = 2;
                                var w = from row in q
                                        where abonentFilter.IsMatch(row[0].ToString())
                                        where row[column].ToString().Length <= 3
                                        where numberFilter.IsMatch(row[column].ToString())
                                        select row;
                                var sum = from e in w.AsEnumerable()
                                          select e[8].ToDecimal();
                                foreach (DataRow row in w)
                                {
                                    row.SetFieldWithColumn(11, "Filtered");
                                }
                                //Debug.WriteLine("Special: " + sum.Sum());
                                resRow.SetFieldWithColumn(index, Math.Round(sum.Sum() / 60, 2));
                                break;
                            }
                        case 4:
                            {
                                abonentFilter = new Regex(@"C12700[0-9]*"); //Фильтр для исходящих абонентов
                                numberFilter = new Regex(@"^8(?!495|499|498|800|10)");//Междугородние
                                column = 2;
                                var w = from row in q
                                        where abonentFilter.IsMatch(row[0].ToString())
                                        where row[column].ToString().Length >= 11
                                        where numberFilter.IsMatch(row[column].ToString())
                                        select row;
                                var sum = from e in w.AsEnumerable()
                                          select e[8].ToDecimal();
                                foreach (DataRow row in w)
                                {
                                    row.SetFieldWithColumn(11, "Filtered");
                                }
                                //Debug.WriteLine("Intercity: " + sum.Sum());
                                resRow.SetFieldWithColumn(index, Math.Round(sum.Sum() / 60, 2));
                                break;
                            }
                        case 5:
                            {
                                abonentFilter = new Regex(@"C12700[0-9]*"); //Фильтр для исходящих абонентов
                                numberFilter = new Regex(@"^(810)");//Международные
                                column = 2;
                                var w = from row in q
                                        where abonentFilter.IsMatch(row[0].ToString())
                                        where row[column].ToString().Length >= 11
                                        where numberFilter.IsMatch(row[column].ToString())
                                        select row;
                                var sum = from e in w.AsEnumerable()
                                          select e[8].ToDecimal();
                                foreach (DataRow row in w)
                                {
                                    row.SetFieldWithColumn(11, "Filtered");
                                }
                                //Debug.WriteLine("International: " + sum.Sum());
                                resRow.SetFieldWithColumn(index, Math.Round(sum.Sum() / 60, 2));
                                break;
                            }
                    }
                }
                DataBase.dataset.Tables["Result"].Rows.Add(resRow);
            }
            if (fl.Count == 5)
            {
                var broken = from b in DataBase.dataset.Tables["Data"].AsEnumerable()
                             where b[11].ToString() != "Filtered"
                             select b;
                //DataBase.AddNewTable("Broken");
                DataTable tbl = DataBase.dataset.Tables["Data"].Clone();
                tbl.TableName = "Broken";
                DataBase.dataset.Tables.Add(tbl);
                foreach (DataRow br in broken)
                {
                    br[11] = "Broken";
                    DataBase.dataset.Tables["Broken"].ImportRow(br);
                }
            }
            //Debug.WriteLine(DataBase.dataset.GetXml());
        }

       
    }
}

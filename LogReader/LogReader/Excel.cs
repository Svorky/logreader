﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace LogReader
{
    class Excel
    {
        public static Dictionary<int, IEnumerable<DataRow>> filteredStrings = new Dictionary<int, IEnumerable<DataRow>>();
        public static string Export()
        {
            if (DataBase.dataset.Tables["Result"].AsEnumerable().First()[0].ToString().Split('-')[1] != DataBase.dataset.Tables["Result"].AsEnumerable().Last()[0].ToString().Split('-')[1])
            {
                DataBase.dataset.Tables["Result"].Rows.RemoveAt(0);
                DataBase.dataset.Tables["Result"].AcceptChanges();
            }
            string result = "Date";
            foreach (var q in DataParser.fl)
            {
                result += ";" + (int) q;
            }
            result += "\r";
            foreach (DataRow r in DataBase.dataset.Tables["Result"].Rows)
            {
                foreach (var item in r.ItemArray)
                {
                    result += item + ";";
                }
                result += "\r";
            }
            result += "\rTotal:;";
            foreach (decimal i in Sum().Skip(1))
            {
                result += i + ";";
            }
            return result;
        }

        private static List<decimal> Sum()
        {
            List<decimal> s = new List<decimal>();

            foreach (DataColumn dc in DataBase.dataset.Tables["Result"].Columns)
            {
                decimal sum = 0;
                foreach (DataRow dr in DataBase.dataset.Tables["Result"].Rows)
                {
                    decimal number = 0;
                    bool res = decimal.TryParse(dr[dc].ToString(), out number);
                    if (res) sum += number;
                }
                s.Add(sum);
            }
            return s;
        }

        public static string Export(DataView view, string filter)
        {
            view.RowFilter = filter;
            string res = null;
            foreach (DataRowView row in view)
            {
                foreach (var item in row.Row.ItemArray)
                {
                    res += item.ToString() + ";";
                }
                res += "\r";
            }
            return res;
        }

        public static string Export(DataRow[] rows)
        {
            string res = null;
            foreach (DataRow row in rows)
            {
                foreach (var item in row.ItemArray)
                {
                    res += item.ToString() + ";";
                }
                res += "\r";
            }
            return res;
        }

        public static string Export(DataRowCollection rows)
        {
            string res = null;
            foreach (DataRow row in rows)
            {
                foreach (var item in row.ItemArray)
                {
                    res += item.ToString() + ";";
                }
                res += "\r";
            }
            return res;
        }

        public static string Export(IEnumerable<DataRow> rows)
        {
            string res = null;
            foreach (DataRow row in rows)
            {
                foreach (var item in row.ItemArray)
                {
                    res += item.ToString() + ";";
                }
                res += "\r";
            }
            return res;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Extensions
{
    public static class Extension
    {
        /// <summary>
        /// Суммирует инты в листе
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static int Sum(this List<int> list)
        {
            int total = 0;
            foreach (int item in list)
            {
                total += item;
            }
            return total;
        }
        /// <summary>
        /// Перевод String в Integer. Author: AlexKurk
        /// </summary>
        /// <param name="str">строка для конвертирования</param>
        /// <returns>Integer</returns>
        /// <remarks>Alexander Kurkovsky</remarks>
        public static int ToInt(this string str)
        {
            return int.Parse(str);
        }
        /// <summary>
        /// Перевод Object в Integer. Если obj равен null, возвращает -1.
        /// </summary>
        /// <param name="obj">объект</param>
        /// <returns>int</returns>
        public static int ToInt(this object obj)
        {
            if (obj == null)
            {
                return -1;
            }
            else return obj.ToString().ToInt();
        }

        public static int ToIntIfNullOrEmpty(this object obj, int returned)
        {
            if (obj == null || obj.ToString() == "")
            {
                return returned;
            }
            else return obj.ToInt();
        }
        /// <summary>
        /// Перевод строки в децимал.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static decimal ToDecimal(this string str)
        {
            return decimal.Parse(str);
        }
        /// <summary>
        /// Перевод объекта в децимал. Если null, то вернет 0.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static decimal ToDecimal(this object obj)
        {
            if (obj == null)
            {
                return 0;
            }
            else return obj.ToString().ToDecimal();
        }
        /// <summary>
        /// Добавляет значение в конструкцию List&lt;List&lt;V&gt;&gt;
        /// </summary>
        /// <typeparam name="K">Ключ</typeparam>
        /// <typeparam name="V">Значение</typeparam>
        /// <param name="dict"></param>
        /// <param name="key">Ключ</param>
        /// <param name="val">Значение</param>
        public static void AddValueToInnerList<K, V>(this Dictionary<K, List<V>> dict, K key, V val)
        {
            List<V> list;
            if (!dict.TryGetValue(key, out list))
                dict[key] = list = new List<V>();
            list.Add(val);
        }
        /// <summary>
        /// Вставляет значение в ячейку по индексу. Если колонки не существует, она будет создана, но не по значению индекса, а будет вставлена после последней колонки.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="row"></param>
        /// <param name="index"></param>
        /// <param name="value"></param>
        public static void SetFieldWithColumn<T>(this DataRow row, int index, T value)
        {
            try
            {
                row.SetField(index, value);
            }
            catch (IndexOutOfRangeException)
            {
                row.Table.Columns.Add();
                row.SetFieldWithColumn(index, value);
            }
        }

        public static string getTableName(this DataRowCollection drc)
        {
            return drc[0].Table.TableName;
        }
    }
}

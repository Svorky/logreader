﻿using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using Extensions;
using System.Linq;

namespace LogReader
{
    class DataBase
    {
        public static DataSet dataset = new DataSet();

        public static void AddNewTable(string name)
        {
            dataset.Tables.Add(name);
        }

        public static void ClearDataTable(string name)
        {
            dataset.Tables[name].Clear();
        }

        public static void CreateAccessDB(string filename)
        {
            byte[] data = Properties.Resources.db1;
            BinaryWriter bw = new BinaryWriter(File.Open(Path.GetDirectoryName(filename) + "/" +Path.GetFileNameWithoutExtension(filename)+".mdb", FileMode.Create));
            bw.Write(data);
            bw.Flush();
            bw.Close();
        }
        public static void ExportToDataBase(string filename, string newfilename, IEnumerable<DataRow> rows)
        {
            OleDbConnection myConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source="+Path.GetDirectoryName(filename) + "/" +Path.GetFileNameWithoutExtension(filename)+".mdb;");

            //command to insert each ASIN
            OleDbCommand cmd = new OleDbCommand();

            // Now we will collect data from data table and insert it into database one by one
            // Initially there will be no data in database so we will insert data in first two columns
            // and after that we will update data in same row for remaining columns
            // The logic is simple. 'i' represents rows while 'j' represents columns

            cmd.Connection = myConnection;
            cmd.CommandType = CommandType.Text;

            myConnection.Open();
            string columnsName = null;

            foreach (DataColumn cln in rows.First().Table.Columns)
            {
                columnsName += "[" + cln.ColumnName + "] TEXT, ";
            }
            columnsName = columnsName.TrimEnd(',', ' ');
            cmd.CommandText = "CREATE TABLE " + newfilename + " (" + columnsName + ");";
            cmd.ExecuteNonQuery();
            foreach (DataRow row in rows)
            {
                cmd.CommandText = "INSERT INTO " + newfilename + " VALUES (";
                foreach (string item in row.ItemArray)
                {
                    cmd.CommandText += "'" + item + "', ";
                }
                cmd.CommandText = cmd.CommandText.TrimEnd(',', ' ');
                cmd.CommandText += ");";
                cmd.ExecuteNonQuery();
            }
            cmd.Connection.Close();
        }

        public static void ExportToDataBase(string filename, DataRowCollection rowCol)
        {
            OleDbConnection myConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source="+Path.GetDirectoryName(filename) + "/" +Path.GetFileNameWithoutExtension(filename)+".mdb;");

            //command to insert each ASIN
            OleDbCommand cmd = new OleDbCommand();

            // Now we will collect data from data table and insert it into database one by one
            // Initially there will be no data in database so we will insert data in first two columns
            // and after that we will update data in same row for remaining columns
            // The logic is simple. 'i' represents rows while 'j' represents columns

            cmd.Connection = myConnection;
            cmd.CommandType = CommandType.Text;

            myConnection.Open();
            string columnsName = null;
            foreach (DataColumn cln in dataset.Tables["Data"].Columns)
            {
                columnsName += "[" + cln.ColumnName + "] TEXT, ";
            }
            columnsName = columnsName.TrimEnd(',', ' ');
            cmd.CommandText = "CREATE TABLE " + rowCol[0].Table.TableName + " (" + columnsName + ");";
            cmd.ExecuteNonQuery();
            foreach (DataRow row in rowCol)
            {
                cmd.CommandText = "INSERT INTO " + rowCol.getTableName() + " VALUES (";
                foreach (string item in row.ItemArray)
                {
                    cmd.CommandText += "'" + item + "', ";
                }
                cmd.CommandText = cmd.CommandText.TrimEnd(',', ' ');
                cmd.CommandText += ");";
                cmd.ExecuteNonQuery();
            }
            cmd.Connection.Close();
        }
    }
}

﻿//Reader for log files from cdr
//
// Author
// Name                     E-mail
// ===================      =============================
// Alexander Kurkovsky      alexander.kurkovsky@yandex.ru
//
// Modifications
// Date         Author                  Description
// ==========   ===================     ======================
// 27.07.2015   Alexander Kurkovsky     Creation
// 31.07.2015   Alexander Kurkovsky     Realese
// 21.08.2015   Alexander Kurkovsky     -Cosmetic
//                                      -Focus on file box in file open dialog
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;
using System.Data;
using Extensions;
using System.Threading.Tasks;
using System.ComponentModel;

namespace LogReader
{
    public partial class Form1 : Form
    {
        private string[] filenames;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Multiselect = true;
            openFileDialog1.Filter = "log files (*.log)|*.log";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            // Set Minimum to 1 to represent the first file being copied.
            progressBar1.Minimum = 1;
            // Set the initial value of the ProgressBar.
            progressBar1.Value = 1;
            // Set the Step property to a value of 1 to represent each file being copied.
            progressBar1.Step = 1;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filenames = openFileDialog1.FileNames;
                //Очищаем таблицу
                //Excel.table.Reset();
                DataBase.dataset.Reset();
                DataBase.AddNewTable("Data");
                l_AllStrings.Text = "Всего строк: ";
                l_StringsParsed.Text = "Не обработано строк: ";
                //label2.Invoke((MethodInvoker) delegate { label2.Text = "Идет обработка"; });
                b_Start.Enabled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();


        }

        private void saveFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                backgroundWorker1.RunWorkerAsync();
                button1.Enabled = false;
                button2.Enabled = false;
                b_Start.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
            //DataBase.ClearDataTable("Data");
        }

        private void cb_InputCalls_CheckedChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
            if (DataParser.fl.Contains(DataParser.Filter.InputCalls))
            {
                DataParser.fl.Remove(DataParser.Filter.InputCalls);
            }
            else
            {
                DataParser.fl.Add(DataParser.Filter.InputCalls);
            }

        }

        private void cb_OutputCity_CheckedChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
            if (DataParser.fl.Contains(DataParser.Filter.OutputCityCalls))
            {
                DataParser.fl.Remove(DataParser.Filter.OutputCityCalls);
            }
            else
            {
                DataParser.fl.Add(DataParser.Filter.OutputCityCalls);
            }
        }

        private void cb_SpecialService_CheckedChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
            if (DataParser.fl.Contains(DataParser.Filter.SpecialServiceCalls))
            {
                DataParser.fl.Remove(DataParser.Filter.SpecialServiceCalls);
            }
            else
            {
                DataParser.fl.Add(DataParser.Filter.SpecialServiceCalls);
            }
        }

        private void cb_OutputIntercity_CheckedChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
            if (DataParser.fl.Contains(DataParser.Filter.OutputCrossCityCalls))
            {
                DataParser.fl.Remove(DataParser.Filter.OutputCrossCityCalls);
            }
            else
            {
                DataParser.fl.Add(DataParser.Filter.OutputCrossCityCalls);
            }
        }

        private void cb_OutputInternational_CheckedChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
            if (DataParser.fl.Contains(DataParser.Filter.OutputCrossCountryCalls))
            {
                DataParser.fl.Remove(DataParser.Filter.OutputCrossCountryCalls);
            }
            else
            {
                DataParser.fl.Add(DataParser.Filter.OutputCrossCountryCalls);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            cb_InputCalls.Checked = true;
            cb_OutputCity.Checked = true;
            cb_SpecialService.Checked = true;
            cb_OutputIntercity.Checked = true;
            cb_OutputInternational.Checked = true;
        }

        private void b_Start_Click(object sender, EventArgs e)
        {
            label2.Text = "Идет считывание файлов";
            label2.Visible = true;
            label2.Refresh();

            progressBar1.Visible = true;
            // Set Maximum to the total number of files to copy.
            progressBar1.Maximum = filenames.Length;
            int progressValue = 0;
            foreach (String filename in filenames)
            {
                try
                {
                    Worker.doStart(filename);
                    progressBar1.PerformStep();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message + ex.StackTrace);
                }
                progressValue++;
            }
            label2.Text = "Идёт обработка данных";
            DataParser.Parser();

            label2.Text = "Завершено";
            if (DataParser.fl.Count == 5)
            {
                l_StringsParsed.Text += DataBase.dataset.Tables["Broken"].Rows.Count;
                l_AllStrings.Text += DataBase.dataset.Tables["Data"].Rows.Count;
            }
            button2.Enabled = true;

        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            
            Worker.Saver(saveFileDialog1.FileName, cb_Debug.Checked);


        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // First, handle the case where an exception was thrown.
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message + e.Error.StackTrace);
            }
            else
            {
                // Finally, handle the case where the operation 
                // succeeded.
                MessageBox.Show("Обработка закончена");
                this.button1.Enabled = true;
                //DataBase.dataset.Reset();

            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Extensions;

namespace LogReader
{
    class Worker
    {
        public static void doStart(string filename)
        {
            string line = "";
            System.IO.StreamReader file = new System.IO.StreamReader(filename);
            while ((line = file.ReadLine()) != null)
            {
                Regex Reg = new Regex(@"\S+"); //Ищем все символы, которые стоят подряд, не пробелы
                DataRow row = DataBase.dataset.Tables["Data"].NewRow();
                int index = 0;
                foreach (Match m in Reg.Matches(line))
                {
                    row.SetFieldWithColumn(index, m.ToString());
                    index++;
                }
                DataBase.dataset.Tables["Data"].Rows.Add(row);
            }
            file.Close();
        }

        public static void Saver(string name, bool debug)
        {
            File.WriteAllText(name, Excel.Export());
            if (debug == true)
            {
                DataBase.CreateAccessDB(name);
                DataBase.ExportToDataBase(name, DataBase.dataset.Tables["Broken"].Rows);
                DataBase.ExportToDataBase(name, "1_Входящие_от_МГТС", DataBase.dataset.Tables["Data"].AsEnumerable().Where(
                        x => new Regex(@"^C12701[0-9]\d+").IsMatch(x[0].ToString())
                        )
                    );
                DataBase.ExportToDataBase(name, "2_Исходящие_от_МГТС", DataBase.dataset.Tables["Data"].AsEnumerable().Where(
                        x => new Regex(@"C12700[0-9]*").IsMatch(x[0].ToString())
                        ).Where(
                        row => row[2].ToString().Length >= 11 || row[2].ToString().Length == 3
                        ).Where(
                        x => new Regex(@"^(100|8495|8498|8499|8800)").IsMatch(x[2].ToString())
                        )
                    );
                DataBase.ExportToDataBase(name, "3_Спецслужбы", DataBase.dataset.Tables["Data"].AsEnumerable().Where(
                        x => new Regex(@"C12700[0-9]*").IsMatch(x[0].ToString())
                        ).Where(
                        row => row[2].ToString().Length <= 3
                         ).Where(
                         x => new Regex(@"^(01|02|03|04|101|102|103|104)").IsMatch(x[2].ToString())
                        )
                    );
                DataBase.ExportToDataBase(name, "4_Исходящие_междугородние", DataBase.dataset.Tables["Data"].AsEnumerable().Where(
                        x => new Regex(@"C12700[0-9]*").IsMatch(x[0].ToString())
                        ).Where(
                        row => row[2].ToString().Length >= 11
                        ).Where(
                        x => new Regex(@"^8(?!495|499|498|800|10)").IsMatch(x[2].ToString())
                        )
                    );
                DataBase.ExportToDataBase(name, "5_Исходящие_международные", DataBase.dataset.Tables["Data"].AsEnumerable().Where(
                        x => new Regex(@"C12700[0-9]*").IsMatch(x[0].ToString())
                        ).Where(
                        row => row[2].ToString().Length >= 11
                        ).Where(
                        x => new Regex(@"^(810)").IsMatch(x[2].ToString())
                        )
                    );
            }
        }
    }
}
